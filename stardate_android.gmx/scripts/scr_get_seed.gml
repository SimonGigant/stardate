///scr_get_seed()
randomize();
//Récupère un numéro de ligne aléatoire entre 0 et nb_race-1 :
//Tout d'abord on ajoute les chances de TOUTES les races possibles, regroupées dans un tableau :
sum_luck = 0;
for(i=0;i<nb_race;i++){
    sum_luck += race[i,LUCK];
}
luck = irandom(sum_luck);
sum_luck = 0;
for(i=0;i<nb_race;i++){
    sum_luck += race[i,LUCK];
    if(sum_luck >= luck){
        seed = race[i,NUM];
        break;
    }
}
return seed;

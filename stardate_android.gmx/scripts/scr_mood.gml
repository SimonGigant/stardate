///scr_mood()
//gere le mood de lenemy
//happy : tout est rouge -> 10% de degats
//neutral : defaut -> 50% de degats
//angry : tout est vert -> 100% de degats

nb_red = 0;
nb_green = 0;

for(i=0;i<obj_enemy.block_nb;i+=1){
    if(obj_enemy.block[i,COLOR] == RED){nb_red += 1;}
    if(obj_enemy.block[i,COLOR] == GREEN) {nb_green += 1;}
}


obj_enemy.mood = 0.5

if(nb_red == obj_enemy.block_nb){
    obj_enemy.mood = 0.1
}
if(nb_green == obj_enemy.block_nb){
    obj_enemy.mood = 1.0
}

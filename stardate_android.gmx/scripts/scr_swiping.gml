///scr_swiping()
l_click = mouse_check_button(mb_left);
if(!l_click){
    state = scr_ds_return_initial_position;
}else{
    x_mouse_offset = mouse_x - (start_click_x+x_total_offset);
    x += x_mouse_offset;
    x_total_offset += x_mouse_offset;
}

if(x + 128 > WIDTH){
    state = scr_swiped_right;
}
if(x + 128 < 0){
    if(obj_ds_parameters.left_swipe_allowed > 0){
        obj_ds_parameters.left_swipe_allowed--;
        state = scr_swiped_left;
    }
}

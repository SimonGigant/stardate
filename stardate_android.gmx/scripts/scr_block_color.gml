///scr_block_color()

//gere les changements de couleurs des blocs

cd_color = 5; //cooldown avant de changer de couleur

for (i=0; i < obj_enemy.block_nb ; i += 1) {
    if(obj_enemy.block[i,COLOR] == RED){
        if(timer[i] == 0){
            timer[i] = cd_color;
        }
        timer[i] -= 1;
    if(timer[i] == 0){
        obj_enemy.block[i,COLOR] = YELLOW;
        timer[i] = cd_color;
    }}
    else{
        if(obj_enemy.block[i,COLOR] == YELLOW){
            timer[i] -= 1;   
        }
        if(timer[i]<= 0){
            obj_enemy.block[i,COLOR] = GREEN;
            timer[i] = -1;
        }
    }

}

///scr_stats_flafoune1()
if(instance_exists(obj_enemy)){
    with(obj_enemy){
        num = 1;
        sprite_idle = spr_flafoune_test;
        sprite_angry = spr_flafoune_angry;
        sprite_happy = spr_flafoune_happy;
        alpha_sprite = 1;
        
        //stats globales :
        hp = 50; //hp_max
        atk = 8; //attack (composante aleatoire : de 0 à atk)
        atk_min = 2; //attack mini (composante constante)
        hl = 5; //heal (composante aleatoire : de 0 à hl)
        hl_min = 0; //heal mini (composante constante)
        def_min = 0; //defense de base (sans les bonus)
        
        block_nb = 3;
        
        //Tête :
        emoji_anatomy[0] = spr_emoji_head;
        block[0,X] = 150;
        block[0,Y] = 96;
        block[0,SIZE] = BIG;
        block[0,TYPE] = PROTECTOR;
        block[0,COLOR] = GREEN;
        
        //Trompe :
        emoji_anatomy[1] = spr_emoji_trunk;
        block[1,X] = 120;
        block[1,Y] = 140;
        block[1,SIZE] = MEDIUM;
        block[1,TYPE] = CORE;
        block[1,COLOR] = GREEN;
        
        //Joue :
        emoji_anatomy[2] = spr_emoji_jowl;
        block[2,X] = 176;
        block[2,Y] = 110;
        block[2,SIZE] = SMALL;
        block[2,TYPE] = DEFAULT;
        block[2,COLOR] = GREEN;

    }
}
if(instance_exists(obj_ds_parameters)){
    with(obj_ds_parameters){
        num = 1;
        sprite_picture = spr_picture_test;
    }
}

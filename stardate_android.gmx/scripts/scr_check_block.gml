///scr_check_block();
with(obj_dating){
    target_s = noone;
    target_m = noone;
    target_b = noone;
    target = noone;
}
with(obj_select_block){
    if(position_meeting(mouse_x,mouse_y,self)){
        if(obj_enemy.block[i_target,SIZE]==SMALL){
            obj_dating.target_s = self;
        }else if(obj_enemy.block[i_target,SIZE]==MEDIUM){
            obj_dating.target_m = self;
        }else{
            obj_dating.target_b = self;
        }
    }
}
with(obj_dating){
    if(target_s != noone){
        target = target_s;
    }else if(target_m != noone){
        target = target_m;
    }else if(target_b != noone){
        target = target_b;
    }
    i_target = target.i_target;
}
return obj_dating.i_target;

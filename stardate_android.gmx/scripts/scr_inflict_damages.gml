///scr_inflict_damages('fail' or 'done')


with(obj_enemy){n_emoji = 0;}
player_bubble = instance_create(140,510,obj_speech_bubble);

if(obj_dating.action == ACTION_JOKE){
    action = ACTION_JOKE;
    emoji_joke = instance_create(player_bubble.x-105,player_bubble.y-16,obj_emoji);
    emoji_joke.sprite_index = spr_emoji_smile;
    emoji_joke.visible = false;
}else if(obj_dating.action == ACTION_INSULT){
    action = ACTION_INSULT;
    emoji_insult = instance_create(player_bubble.x-105,player_bubble.y-16,obj_emoji);;
    emoji_insult.sprite_index = spr_emoji_jpp;
    emoji_insult.visible = false;
}else if(obj_dating.action == ACTION_COMPLIMENT){
    action = ACTION_COMPLIMENT;
    emoji_compliment = instance_create(player_bubble.x-105,player_bubble.y-16,obj_emoji);
    emoji_compliment.sprite_index = spr_emoji_nickel;
    emoji_compliment.visible = false;
}

emoji_anatomy = instance_create(player_bubble.x-105+32,player_bubble.y-16,obj_emoji);
emoji_anatomy.sprite_index = obj_enemy.emoji_anatomy[obj_dating.i_target];
emoji_anatomy.visible = false;

//valeur aleatoire

randomize();
random_amount = irandom(5);

//effets selon l'action choisie
//si raté : 0 degats

if(argument0 == 'fail'){
    randomize();
    random_amount = irandom(5);
    damages = 0;
    soins = 0;
    explosion = instance_create(obj_enemy.block[X,obj_dating.i_target]-5+irandom(10),obj_enemy.block[Y,obj_dating.i_target]-5+irandom(10),obj_damage_particles);
    explosion.image_xscale = 1 + random_amount/7 + irandom(10)/50;
    explosion.image_yscale = explosion.image_xscale;
    scr_display_damages(explosion,'miss');
    with(obj_enemy){scr_add_emoji(spr_emoji_default);
        scr_add_emoji(spr_emoji_brain);
        scr_add_emoji(spr_emoji_tentacle);}
}
else{
if(obj_dating.action == ACTION_COMPLIMENT){
    damages = int64(( 6 + random_amount ) * obj_enemy.block[obj_dating.i_target,COLOR] * (1 - def) );
    soins = 0;
    if(damages>0){
        with(obj_enemy){scr_add_emoji(spr_emoji_smile);}
    }
}
if(obj_dating.action == ACTION_JOKE){
    if( obj_enemy.block[obj_dating.i_target,TYPE] == PROTECTOR){
    damages = int64((random_amount + 40) * obj_enemy.block[obj_dating.i_target,COLOR]);
    obj_enemy.block[obj_dating.i_target,TYPE] = BROKEN
    }
    else{
    damages = 0;
    obj_enemy.block[obj_dating.i_target,COLOR] = RED ;

    }
    soins = 0;
    if(damages>1){
        with(obj_enemy){scr_add_emoji(spr_emoji_jpp);}
    }
}
if(obj_dating.action == ACTION_INSULT){
    damages = -random_amount;
    soins = -random_amount * 5 - 10;
    if(soins>-4){
        with(obj_enemy){scr_add_emoji(spr_emoji_hate);}
    }
    for (i = 0; i< obj_enemy.block_nb ; i += 1) {
            obj_enemy.block[i,COLOR] = GREEN;
    }
}
}


//satisfaction

if(damages > 0){
    obj_enemy.satisfaction = 1;
}
else {obj_enemy.satisfaction = -1;}


if(obj_enemy.hp - damages > 0){
    obj_enemy.hp -= damages;
    if(damages>0){
        explosion = instance_create(obj_enemy.block[X,obj_dating.i_target]-5+irandom(10),obj_enemy.block[Y,obj_dating.i_target]-5+irandom(10),obj_damage_particles);
        with(obj_enemy){scr_add_emoji(spr_emoji_jpp);}
    }
    else if (damages == 0)
    {
    }
    else{
        explosion = instance_create(obj_enemy.block[X,obj_dating.i_target]-5+irandom(10),obj_enemy.block[Y,obj_dating.i_target]-5+irandom(10),obj_heal_particles);
        with(obj_enemy){scr_add_emoji(spr_emoji_smile);}
    }
    if(damages != 0){
        explosion.image_xscale = 1 + random_amount/7 + irandom(10)/50;
        explosion.image_yscale = explosion.image_xscale;
        scr_display_damages(explosion,damages);}
}else{
    obj_enemy.hp = 0;
}


obj_player_stat.hp -= soins
if(soins < 0){
    explosion = instance_create(16-5+irandom(10),544-5+irandom(10),obj_heal_particles);
    explosion.image_xscale = 1 + random_amount/7 + irandom(10)/50;
    explosion.image_yscale = explosion.image_xscale;
    soins = -soins;
}
else if (soins == 0)
{
}
else{
    explosion = instance_create(16-5+irandom(10),544-5+irandom(10),obj_damage_particles);
    explosion.image_xscale = 1 + random_amount/7 + irandom(10)/50;
    explosion.image_yscale = explosion.image_xscale;
}
if(soins != 0){
    scr_display_damages(explosion,soins);}

scr_block_color();
scr_mood();


///scr_block_types()

//PROTECTOR
protect = false;
for(i=0;i<obj_enemy.block_nb;i+=1){
    if(obj_enemy.block[i,TYPE] == PROTECTOR){
        protect = true
    }
}
obj_enemy.shielded = protect;
def = obj_enemy.def_min;
if(protect){
    def += 0.5;
}
else{
    def += 0.1;
}

//CORE

duree_rouge_core = 3;

active_core = false
for(i=0;i<obj_enemy.block_nb;i+=1){
    if(obj_enemy.block[i,TYPE] == CORE ){
        switch (obj_enemy.block[i,COLOR]){
            case GREEN : //soigne le mojo ; tous les bodyparts -> green
                heal = -irandom(6 * obj_enemy.hl) - obj_enemy.hl_min * 3;
                obj_enemy.hp -= heal;
                explosion = instance_create(obj_enemy.block[i,X]-5+irandom(10),obj_enemy.block[i,Y]-5+irandom(10),obj_core_heal_particle);
                scr_display_damages(explosion,heal);
                for(k=0;k<=obj_enemy.block_nb;k+=1){
                    obj_enemy.block[k,COLOR] = GREEN;
                    }
                timer_core = duree_rouge_core;
            break;
            case YELLOW : //def++ , -> green
                def += 0.2;
                timer_core = duree_rouge_core;
                obj_enemy.block[i,COLOR] = GREEN;
            break;
            case RED : //au bout de 2 tours -> yellow
                if(timer_core==0){
                    obj_enemy.block[i,COLOR] = YELLOW;
                    timer_core = duree_rouge_core;
                    break;
                }
                timer_core-=1;
            break;
        }
    }
}


//TELEPATH
bonus_damage = 1;
for(i=0;i<obj_enemy.block_nb;i+=1){
    if(obj_enemy.block[i,TYPE] == TELEPATH && obj_enemy.block[i,COLOR] == GREEN ){
        bonus_damage = 1.5;
    }
}


//LANGUAGE




//def_max

def = min(def, 0.9);

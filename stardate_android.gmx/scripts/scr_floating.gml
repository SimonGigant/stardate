///y = scr_floating(initial_y, frequency, intensity)

//Value examples :
//frequency from 0.5 to 3
//intensity from 1 to 3

float_angle += argument1;
float_angle = float_angle mod 360;
y_pos = argument0+sin(float_angle*pi/180)*argument2;
return y_pos;

///scr_emoji_minigame(attack)

switch(argument0){
    case 'compliment' :
        sprite_emoji = spr_emoji_nickel;
        break;
    case 'joke' :
        sprite_emoji = spr_emoji_jpp;
        break;
    case 'insult' :
        sprite_emoji = spr_emoji_hate;
        break;
    default :
        sprite_emoji = spr_emoji_smile;
        break;
}
if(!instance_exists(obj_emoji_minigame)){
    instance_create(16,352,obj_emoji_minigame);
    with(obj_emoji_minigame){
        sprite_emoji = obj_dating.sprite_emoji;
    }
    }

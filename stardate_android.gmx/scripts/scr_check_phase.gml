///scr_check_phase(phase)
phase = argument0;
l_click = mouse_check_button_pressed(mb_left);

switch(phase){
    case(PHASE_CHOICE):{
        if(l_click && instance_exists(instance_position(mouse_x,mouse_y,obj_bubble_player))){
            phase = PHASE_CHOICE;
            if(!instance_exists(obj_speech_bubble)){
                instance_create(140,510,obj_speech_bubble);
            }
        }
        if(l_click && instance_exists(instance_position(mouse_x,mouse_y,obj_button_parent))){
            phase = PHASE_AIMING;
            obj_dating.action = instance_position(mouse_x,mouse_y,obj_button_parent).action;
            scr_create_blocks();
        }
        break;
    }
    case(PHASE_AIMING):{
        if(l_click && !instance_exists(instance_position(mouse_x,mouse_y,obj_select_block))){
            with(obj_select_block){
                instance_destroy();
            }
            with(obj_type_icon){
                instance_destroy();
            }
            phase = PHASE_CHOICE;
            obj_dating.action = 0;
        }else if(l_click && instance_exists(instance_position(mouse_x,mouse_y,obj_select_block))){
            i_current_block = scr_check_block();
            phase = 5;
            with(obj_select_block){
                instance_destroy();
            }
            with(obj_type_icon){
                instance_destroy();
            }
        }
        break;
    }
    case 5 :{
        scr_chose_action();
        //le reste se passe dans obj_emoji_minigame
        break;
    }
    case(PHASE_FEEDBACK):{
        if(!phase_bool){
            scr_feedback();
            feedback_over = false;
        }
        phase_bool = true;
        if(feedback_over){
            phase = PHASE_ANIMATION;
        }
        break;
    }
    case(PHASE_ANIMATION):{
        if(phase_bool){
        scr_damage_to_player();}
        phase_bool = false;
        if(obj_enemy.hp <= 0){
            phase = PHASE_WINNING;
        }else{
            phase = PHASE_CHOICE;
        }
        break;
    }
    case(PHASE_WINNING):{
        scr_winning();
        break;
    }
}
return phase;

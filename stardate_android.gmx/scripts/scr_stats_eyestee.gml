///scr_stats_eyestee()
if(instance_exists(obj_enemy)){
    with(obj_enemy){
        num = 1;
        sprite_idle = spr_eyestee;
        sprite_angry = spr_eyestee_angry;
        sprite_happy = spr_eyestee_happy;
        sprite_stator = spr_eyestee_stator;
        alpha_sprite = 1;
        image_xscale = 3;
        image_yscale = 3;
        
        //stats globales :
        hp = 80; //hp_max
        atk = 7; //attack (composante aleatoire : de 0 à atk)
        atk_min = 4; //attack mini (composante constante)
        hl = 1; //heal (composante aleatoire : de 0 à hl)
        hl_min = 0; //heal mini (composante constante)
        def_min = 0; //defense de base (sans les bonus)
        
        block_nb = 3;
        
        //Tête :
        emoji_anatomy[0] = spr_emoji_head;
        block[0,X] = 150;
        block[0,Y] = 96;
        block[0,SIZE] = SMALL;
        block[0,TYPE] = DEFAULT;
        block[0,COLOR] = GREEN;
        
        //Book :
        emoji_anatomy[1] = spr_emoji_trunk;
        block[1,X] = 96;
        block[1,Y] = 192;
        block[1,SIZE] = SMALL;
        block[1,TYPE] = PROTECTOR;
        block[1,COLOR] = GREEN;
        
        //Corps :
        emoji_anatomy[2] = spr_emoji_jowl;
        block[2,X] = 88 ;
        block[2,Y] = 180;
        block[2,SIZE] = BIG;
        block[2,TYPE] = DEFAULT;
        block[2,COLOR] = GREEN;

    }
}
if(instance_exists(obj_ds_parameters)){
    with(obj_ds_parameters){
        num = 1;
        sprite_picture = spr_picture_test;
    }
}

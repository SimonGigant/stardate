///scr_swiped_left()
x-=15;
if(x+256<-10){
    new_profile = instance_create(initial_x,initial_y,obj_ds_profile);
    with(new_profile){
        state = scr_ds_profile_created;
        visible = true;
    }

    instance_destroy();
}

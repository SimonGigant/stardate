///scr_initialisation_parameters()
if(!instance_exists(obj_player_stat)){
    player = instance_create(0,0,obj_player_stat);
    //Ici il faudra récupérer les stats du personnage sélectionné dans la sauvegarde
    //Si c'est la première partie, il est initialisé
}

if(!instance_exists(obj_bestiary)){
    bestiary = instance_create(0,0,obj_bestiary);
    //Il faudra récupérer dans la sauvegarde les infos du bestiaire rencontré
    //Si c'est la première partie, il est vide
}

if(!instance_exists(obj_area_parameters)){
    area = instance_create(0,0,obj_area_parameters);
    //Il faudra récupérer dans la sauvegarde le dernier quartier visité, pour y revenir
    //Si c'est la première partie, on initialisera à un endroit
}

///scr_create_blocks()
i_max = obj_enemy.block_nb;
for(i=0;i<i_max;i++){
    x_block = obj_enemy.block[i,X];
    y_block = obj_enemy.block[i,Y];
    size = obj_enemy.block[i,SIZE];
    type = obj_enemy.block[i,TYPE];
    //ici il faudra afficher les effets visuels du type
    
    block = instance_create(x_block,y_block,obj_select_block);
    block.i_target = i;
    if(size == BIG){
        block.sprite_index = spr_big_g_block;
    }else if(size == MEDIUM){
        block.sprite_index = spr_medium_g_block;
    }else{
        block.sprite_index = spr_small_g_block;
    }
}

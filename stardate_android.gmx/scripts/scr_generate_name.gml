///scr_generate_name()
name = "";
randomize();
switch(irandom(8)){
    case(0):{name=name+"A";break;}
    case(1):{name=name+"Z";break;}
    case(2):{name=name+"E";break;}
    case(3):{name=name+"R";break;}
    case(4):{name=name+"W";break;}
    case(5):{name=name+"Q";break;}
    case(6):{name=name+"L";break;}
    case(7):{name=name+"O";break;}
    case(8):{name=name+"Iqq";break;}
}
nb_letters = irandom(7) + 3;
for(i=0;i<nb_letters;i++){
    randomize();
    switch(irandom(20)){
        case(0):name=name+"r";break;
        case(1):name=name+"po";break;
        case(2):name=name+"h";break;
        case(3):name=name+"ij";break;
        case(4):name=name+"ol";break;
        case(5):name=name+"ed";break;
        case(6):name=name+"ass";break;
        case(7):name=name+"eg";break;
        case(8):name=name+"'";break;
        case(9):name=name+"tee";break;
        case(10):name=name+"z";break;
        case(11):name=name+"aq";break;
        case(12):name=name+"om";break;
        case(13):name=name+"mm";break;
        case(14):name=name+" ";break;
        case(15):name=name+"kk";break;
        case(16):name=name+"jk";break;
        case(17):name=name+"ge";break;
        case(18):name=name+"goi";break;
        case(19):name=name+"u";break;
        case(20):name=name+"wiu";break;
    }
}
return name;

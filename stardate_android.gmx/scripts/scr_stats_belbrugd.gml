///scr_stats_belbrugd()
if(instance_exists(obj_enemy)){
    with(obj_enemy){
        num = 2;
        sprite_idle = spr_belbrugd;
        sprite_angry = spr_belbrugd_angry;
        sprite_happy = spr_belbrugd_happy;
        alpha_sprite = 1;
        
        //stats globales :
        hp = 30; //hp_max
        atk = 20; //attack (composante aleatoire : de 0 à atk)
        atk_min = 5; //attack mini (composante constante)
        hl = 1; //heal (composante aleatoire : de 0 à hl)
        hl_min = 0; //heal mini (composante constante)
        def_min = 0; //defense de base (sans les bonus)
        
        block_nb = 2;
        
        //Tête :
        emoji_anatomy[0] = spr_emoji_head;
        block[0,X] = 150;
        block[0,Y] = 96;
        block[0,SIZE] = BIG;
        block[0,TYPE] = TELEPATH;
        block[0,COLOR] = GREEN;
        
        //Nez :
        emoji_anatomy[1] = spr_emoji_nose;
        block[1,X] = 135;
        block[1,Y] = 100;
        block[1,SIZE] = MEDIUM;
        block[1,TYPE] = LANGUAGE;
        block[1,COLOR] = GREEN;
    }
}
if(instance_exists(obj_ds_parameters)){
    with(obj_ds_parameters){
        num = 2;
        sprite_picture = spr_picture_test2;
    }
}

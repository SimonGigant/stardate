///scr_chose_sprite(action)
action = argument0;
if(obj_dating.phase == PHASE_CHOICE){
    sprite_index = sprite_choice;
}else if(obj_dating.phase == PHASE_AIMING){
    if(action == obj_dating.action){
        sprite_index = sprite_selected;
    }else{
        sprite_index = sprite_unselected;
    }
}else if(obj_dating.phase == PHASE_FEEDBACK){
    sprite_index = sprite_unselected;
}

///scr_changing_class()
//Efface le bouton de retour et les bios, et affiche les différentes classes
//temporaire :
obj_player_stat.class_get[1] = true;
obj_player_stat.class_get[2] = true;

j = 0;
for(i=0;i<NUMBER_OF_CLASSES;i++){
    if(obj_player_stat.class_get[i]){
        l = j mod 3; //Le numéro de ligne
        c = j div 3; //Le numéro de colonne
        class_icon = instance_create(64+l*96,224+c*96,obj_class_selectable);
        class_icon.sprite_index = obj_player_stat.class_list[i,SPRITE];
        class_icon.class_number = i;
        j++;
    }
}

///scr_damage_to_player()
    
if(obj_enemy.hp > 0)
{
randomize();
random_amount = irandom(obj_enemy.atk) + obj_enemy.atk_min;
damages = int64(random_amount * obj_enemy.mood * bonus_damage);
if(obj_player_stat.hp - damages > 0){
    obj_player_stat.hp -= damages;
    explosion = instance_create(16-5+irandom(10),544-5+irandom(10),obj_damage_particles);
    explosion.image_xscale = 1 + random_amount/7 + irandom(10)/50;
    explosion.image_yscale = explosion.image_xscale;
    scr_display_damages(explosion,damages);
}else{
    obj_player_stat.hp = 0;
}

scr_block_types();
scr_mood();
}
else{obj_enemy.mood = 0.1;}

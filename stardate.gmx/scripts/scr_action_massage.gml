///scr_action_massage()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(LOVE,4);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_touch;
}
with(obj_jauge_soumission){
    submission -= 3;
}
with(obj_jauge_respect){
    respect += 1;
}
with(obj_jauge_amour){
    love += 4;
}
//effets :

ef_sub = MINUS;
ef_resp = PLUS;
ef_love = PLUS;

///scr_action_joke()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,2);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_pray;
}
with(obj_jauge_soumission){
    submission -= 2;
}
with(obj_jauge_respect){
    respect += 5;
}
with(obj_jauge_amour){
    randomize();
    if(irandom(1)==1){
    
        love += 2;
        with(obj_action_joke) {
            ef_love = PLUS;
        }
    }
    else{
        love -=2;
        with(obj_action_joke) {
            ef_love = MINUS;
        }
    }
}
//effets :

ef_sub = MINUS;
ef_resp = PLUS;


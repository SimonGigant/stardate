///scr_action_abduct()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(SUBMISSION,3);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_slap;
}
with(obj_jauge_soumission){
    submission += 2;
}
with(obj_jauge_respect){
    respect -= 2;
}
with(obj_jauge_amour){
    love -= 2;
}
//effets :

ef_sub = PLUS;
ef_resp = MINUS;
ef_love = MINUS;

///scr_balloon_explode(stat,id_arrow)
randomize();
expl = instance_create(x,y,obj_balloon_explode);
switch(argument0){
    case(RESPECT):{
        expl.sprite_index = spr_yellow_explosion;
        break;
    }
    case(LOVE):{
        expl.sprite_index = spr_pink_explosion;
        break;
    }
    case(SUBMISSION):{
        expl.sprite_index = spr_grey_explosion;
        break;
    }
}
do{
randomize();
next_x = choose(32,288,384,400);
next_y = choose(16,32,230,240);
}until((abs(instance_nearest(next_x,next_y,obj_balloon).x - next_x) > 10 && abs(instance_nearest(next_x,next_y,obj_balloon).y - next_y) > 10));
next_balloon = instance_create(next_x, next_y,obj_balloon);
if(irandom(1) == 0){
    next_balloon.stat = obj_enemy.dominante;
}else{
    next_balloon.stat = irandom(2);
}
next_balloon.bonus = argument1.bonus;
with(argument1){
    instance_destroy();
}
instance_destroy();

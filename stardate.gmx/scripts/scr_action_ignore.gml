///scr_action_ignore()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,4);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_wait;
}
with(obj_jauge_soumission){
    submission -= 1;
}
with(obj_jauge_respect){
    respect += 1;
}
with(obj_jauge_amour){
    love += 0;
}
//effets :

ef_sub = MINUS;
ef_resp = PLUS;
ef_love = EQUAL;

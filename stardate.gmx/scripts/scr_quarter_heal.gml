///scr_quarter_heal(quarter)
healable[argument0] = true;
if(alarm[1+argument0] <= 0){
    with(obj_insect_hooked){
        if (position == argument0){
            obj_wheel.healable[argument0] = false;
        }
    }
    
    if(healable[argument0] && hp[argument0]<hp_max){
        hp[argument0] += 1;
        alarm[argument0 + 1] = 120;
        randomize();
        instance_create(x + h, y, obj_heal_particle);
        with(instance_nearest(x + h,y,obj_heal_particle)){
            randomize();
            x = x +5 - irandom(5);
            y = y +5 - irandom(5);
            scale = random(0.2);
            image_xscale = scale + 0.6;
            image_yscale = scale +0.6;
            image_angle = obj_wheel.image_angle + 90*argument0;
        }
    }
}

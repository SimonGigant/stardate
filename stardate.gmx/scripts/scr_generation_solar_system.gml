///scr_generation_solar_system()
randomize();
instance_create(80+irandom(200),80+irandom(50),obj_sun);

nb_planet = irandom(2)+1;

for(i=1;i<=nb_planet;i+=1){
    randomize();
    x_planet = 30+irandom(300);
    y_planet = 20+irandom(200);
    instance_create(x_planet,y_planet,obj_planet);
    nb_moon = 0
    while(irandom(2) == 0 && nb_moon < 4){
        instance_create(x_planet+irandom(30)-15,y_planet+irandom(30)-15,obj_moon);
        nb_moon +=1;
    }
}

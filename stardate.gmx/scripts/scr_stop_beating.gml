///scr_stop_beating()
if(abs(image_xscale - abs(target_fqc)) <= 1){
    image_xscale = 1;
    image_yscale = 1;
    state = next_state;
    frequency = abs(target_fqc);
}else{
    if(image_xscale >1){
        image_xscale -= abs(target_fqc)/2;
        image_yscale -= abs(target_fqc)/2;
    }else{
        image_xscale += abs(target_fqc)/2;
        image_yscale += abs(target_fqc)/2;
    }
}
scr_turning();

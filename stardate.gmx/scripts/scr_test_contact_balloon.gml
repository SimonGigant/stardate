///scr_test_contact_balloon(stat)
if(place_meeting(x,y,obj_arrow)){
    arrow = instance_nearest(x,y,obj_arrow);
    if(argument0 == RESPECT && arrow.sprite_index == spr_yellow_arrow){
        scr_balloon_explode(argument0, arrow);
    }else if(argument0 == SUBMISSION && arrow.sprite_index == spr_grey_arrow){
        scr_balloon_explode(argument0, arrow);
    }else if(argument0 == LOVE && arrow.sprite_index == spr_pink_arrow){
        scr_balloon_explode(argument0, arrow);
    }
}

///scr_chose_next_action()
n = -1;
deck_possible[0] = obj_action_wait; //Juste pour initialiser
for(i=0 ; i < obj_deck.deck_max ; i+=1){
    if(!instance_exists(obj_deck.deck[i])){
        n+=1;
        deck_possible[n] = obj_deck.deck[i];
    }
}
if(n>0){
    randomize();
    a = irandom(n);
}else{
    a = 0;
}
return deck_possible[a];

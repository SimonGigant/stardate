///scr_action_ddos()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(SUBMISSION,10);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.2;
    current_sprite_action = spr_action_reboot;
}

with(obj_jauge_soumission){
    randomize();
    if(irandom(3)==0){
        submission += 5;
    }else{
        submission += 3;
    }
}
with(obj_jauge_respect){
    randomize();
    if(irandom(3)==0){
        respect -= 7;
    }else{
        respect -= 1;
    }
}
with(obj_jauge_amour){
    love += 0;
}

//effets :

ef_sub = PLUS;
ef_resp = MINUS;
ef_love = EQUAL;

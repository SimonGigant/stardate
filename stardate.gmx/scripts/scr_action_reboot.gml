///scr_action_reboot()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(SUBMISSION,5);

with(obj_wheel_parent){
    if(state = scr_turning){
        state = scr_beating;
    }
    current_sprite_action = spr_action_reboot;
}

with(obj_jauge_soumission){
    if(submission>=80){
        submission += 6;
    }else{
        submission +=1;
    }
}
with(obj_jauge_respect){
    respect -= 2;
}
with(obj_jauge_amour){
    love -= 3;
}

//effets :

ef_sub = PLUS;
ef_resp = MINUS;
ef_love = MINUS;

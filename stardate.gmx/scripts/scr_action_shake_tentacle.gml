///scr_action_shake_tentacle()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,5);

with(obj_wheel_parent){
    current_sprite_action = spr_action_pray;
}

with(obj_jauge_soumission){
    randomize();
    if(irandom(10)==0){
        submission += 18;
        with(obj_action_shake_tentacle){ef_sub = PLUS}
    }else{
        submission -= 4;
        with(obj_action_shake_tentacle){ef_sub = MINUS}
    }
}
with(obj_jauge_respect){
randomize();
    if(irandom(1)==0){
        respect += 10;
        with(obj_action_shake_tentacle){ef_resp = PLUS}
    }else{
        respect += 0
        with(obj_action_shake_tentacle){ef_resp = EQUAL}
    }
}
with(obj_jauge_amour){
    love += 0;
}

//effets :


ef_love = EQUAL;

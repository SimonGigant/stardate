///scr_beat_ungrowing()
if(!growing){
    if(image_xscale<1){
        fqc_accel = fqc_accel*0.1;
        if(fqc_accel < 0.00001){
            growing=true;
        }
        frequency += fqc_accel;
    }else if(frequency != target_fqc){
        fqc_accel = fqc_accel*1.5 - 0.001;
        frequency += fqc_accel;
        if(frequency + target_fqc<=fqc_accel*1.5 - 0.001){
            frequency = target_fqc;
            freq_accel = 0;
        }
    }
}
scr_turning();
if(abs(image_xscale - min_size)>0.01){
    image_xscale += frequency;
    image_yscale += frequency;
}

if(growing){
        frequency = -target_fqc;
        state = scr_beating;
}

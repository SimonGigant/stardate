///scr_action_pray()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,5);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*0.8;
    current_sprite_action = spr_action_pray;
}

with(obj_jauge_soumission){
    submission -= 7;
}
with(obj_jauge_respect){
    respect += 6;
}
with(obj_jauge_amour){
    love += 0;
}

//effets :

ef_sub = MINUS;
ef_resp = PLUS;
ef_love = EQUAL;

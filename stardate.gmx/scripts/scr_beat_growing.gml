///scr_beat_growing()
if(frequency != target_fqc && growing){
    fqc_accel = fqc_accel*3.1 + 0.01;
    frequency += fqc_accel;
    if(abs(frequency - target_fqc) <= fqc_accel*3.1 + 0.01){
        frequency = target_fqc;
    }
}else if(second_beat && !growing){
    if(abs(image_xscale - max_size) > 0.1){
        growing = true;
        frequency = 0;
        fqc_accel = 0.008;
    }else{
        frequency = -0.03;
    }
}else if(image_xscale == max_size){
    if(second_beat){
        state = scr_beat_ungrowing;
        frequency = -0.001;
        fqc_accel = 0;
        target_fqc = -target_fqc;
        growing = false;
    }else{
        second_beat = true;
        growing = false;
    }
}
scr_turning();
image_xscale += frequency;
image_yscale += frequency;

if(growing && abs(image_xscale-max_size)<=0.03){
    image_xscale = max_size;
    image_yscale = max_size;
}

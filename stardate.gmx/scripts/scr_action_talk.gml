///scr_action_talk()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,2);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_pray;
}
with(obj_jauge_soumission){
    submission -= 0;
}
with(obj_jauge_respect){
    respect += 3;
}
with(obj_jauge_amour){
    love += 0;
}
//effets :

ef_sub = EQUAL;
ef_resp = PLUS;
ef_love = EQUAL;

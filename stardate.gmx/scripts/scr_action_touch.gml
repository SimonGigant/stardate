///scr_action_touch()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(LOVE,1);

with(obj_wheel_parent){
    if(state = scr_turning){
        state = scr_beating;
    }else{
        fqc_parameter = 0.04;
    }
    current_sprite_action = spr_action_touch;
}
with(obj_jauge_soumission){
    submission -= 0;
}
with(obj_jauge_respect){
    respect -= 2;
}
with(obj_jauge_amour){
    love += 5;
}
//effets :

ef_sub = EQUAL;
ef_resp = MINUS;
ef_love = PLUS;

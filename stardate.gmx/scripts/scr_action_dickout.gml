///scr_action_dickout()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(SUBMISSION,10);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_wait;
}
with(obj_jauge_soumission){
    submission += 0;
}
with(obj_jauge_respect){
    respect -= 5;
}
with(obj_jauge_amour){
    love -= 0;
}
//effets :

ef_sub = PLUS;
ef_resp = MINUS;
ef_love = EQUAL;

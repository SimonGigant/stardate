///scr_action_wait()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,3);

with(obj_wheel_parent){
    scr_changing_direction();
    current_sprite_action = spr_action_wait;
}

with(obj_jauge_soumission){
    submission -= 3;
}
with(obj_jauge_respect){
    respect += 2;
}
with(obj_jauge_amour){
    love += 0;
}

//effets :

ef_sub = MINUS;
ef_resp = PLUS;
ef_love = EQUAL;

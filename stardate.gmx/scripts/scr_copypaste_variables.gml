//scr_copypaste_variables(obj_copied, obj_pasted);
pasted = argument1;
with(pasted){
    copied = argument0;
    state = copied.state;
    spd_parameter = copied.spd_parameter;
    fqc_parameter = copied.fqc_parameter;
    spd = copied.spd;
    frequency = copied.frequency;
    changing_dir = copied.changing_dir;
    next_state = copied.next_state;
    acceleration = copied.acceleration;
    target_spd = copied.target_spd;
    fqc_accel = copied.fqc_accel;
    target_fqc = copied.target_fqc;
    sign_spd = copied.sign_spd;
    sign_fqc = copied.sign_fqc;
    growing = copied.growing;
    second_beat = copied.second_beat;
    max_size = copied.max_size;
    min_size = copied.min_size;
    image_xscale = copied.image_xscale;
    image_yscale = copied.image_yscale;
    image_angle = copied.image_angle;
    position = copied.position;
}

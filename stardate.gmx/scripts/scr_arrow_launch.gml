///scr_arrow_launch(stat, bonus)
arrow = instance_create(obj_wheel.x,obj_wheel.y,obj_arrow);
arrow.dir = image_angle+45;
switch (argument0){
    case (LOVE):{
        arrow.sprite_index = spr_pink_arrow;
        break;
    }
    case (SUBMISSION):{
        arrow.sprite_index = spr_grey_arrow;
        break;
    }
    case (RESPECT):arrow.sprite_index = spr_yellow_arrow;
}
arrow.bonus = argument1;

///scr_action_bend_knee()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(LOVE,3);

with(obj_wheel_parent){
    spd_parameter = spd_parameter*1.1;
    current_sprite_action = spr_action_pray;
}
with(obj_jauge_soumission){
    submission -= 4;
}
with(obj_jauge_respect){
    respect += 0;
}
with(obj_jauge_amour){
    love += 5;
}
//effets :

ef_sub = MINUS;
ef_resp = EQUAL;
ef_love = PLUS;

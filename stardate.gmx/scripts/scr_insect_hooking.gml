///scr_insect_hooking()
mx = (x+obj_wheel.x)/2; //It helps to select the right quarter
my = (y+obj_wheel.y)/2;
if(position_meeting(mx,my,obj_action_parent) && !position_meeting(mx,my,obj_quarter_locked)){
    inst = instance_create(x,y,obj_insect_hooked);
    with(inst){
        //Uses maths to find the angle :
        cosinus = (x - obj_wheel.x)/(round(obj_wheel.halfsize*obj_wheel.image_xscale));
        if(abs(cosinus)>1){
            cosinus = sign(cosinus);
        }
        sinus = (obj_wheel.y - y)/round(obj_wheel.halfsize*obj_wheel.image_yscale);
        angle = round(180*arccos(cosinus)/pi);
        if(sinus<0){
            angle = 360 - angle;
        }
        while(angle<0){
            angle+=360;
        }
        while(angle>360){
            angle-=360;
        }
        if(angle>obj_wheel.image_angle){
            position = ((scr_mod(angle - obj_wheel.image_angle,360)) div 90);
            angle_diff = scr_mod(angle - obj_wheel.image_angle,360);
        }else{
            position = ((scr_mod(360 + angle - obj_wheel.image_angle,360)) div 90);
            angle_diff = scr_mod(360 + angle - obj_wheel.image_angle,360);
        }
        position = scr_mod(position,4);
        angle_diff = scr_mod(angle_diff,360);
        angle_diff-=position*90;
        angle_diff = scr_mod(angle_diff,360);
    }
    instance_destroy();
}

///scr_quarter_locked(quarter)
with(obj_insect_hooked){
    if(position == argument0){
        scr_insect_kill();
    }
}
with(obj_action_parent){
    if(position == argument0){
        scr_quarter_lock();
    }
}

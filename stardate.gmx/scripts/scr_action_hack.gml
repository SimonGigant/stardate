///scr_action_hack()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(SUBMISSION,2);

with(obj_wheel_parent){
    fqc_parameter = fqc_parameter*1.12;
    current_sprite_action = spr_action_reboot;
}

with(obj_jauge_soumission){
    submission += 4;
}
with(obj_jauge_respect){
    respect -= 1;
}
with(obj_jauge_amour){
    love += 0;
}

//effets :

ef_sub = PLUS;
ef_resp = MINUS;
ef_love = EQUAL;

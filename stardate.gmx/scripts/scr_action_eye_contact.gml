///scr_action_eye_contact()

//Ici les effets de cette action sur l'ennemi

with(obj_wheel_parent){
    scr_changing_direction();
    spd_parameter = spd_parameter*0.9;
    current_sprite_action = spr_action_wait;
}
with(obj_jauge_soumission){
    if(submission >= 60){
        submission += 20;
    }
    else {
        submission -=15;
    }
}
with(obj_jauge_respect){
    if(respect >= 60){
        respect += 20;
    }
    else {
        respect -=15;
    }
}
with(obj_jauge_amour){
    if(love >= 60){
        love += 20;
    }
    else {
        love -=15;
    }
}

//effets :

ef_sub = EQUAL;
ef_resp = EQUAL;
ef_love = EQUAL;

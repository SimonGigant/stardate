///scr_insect_move_to_wheel()
dir = point_direction(x,y,obj_wheel.x,obj_wheel.y);
hspd = lengthdir_x(spd, dir);
vspd = lengthdir_y(spd, dir);
image_angle = dir-90;

if(!instance_exists(instance_position(x,y,obj_wheel))){
    if(sqrt((x-obj_wheel.x)*(x-obj_wheel.x)+(y-obj_wheel.y)*(y-obj_wheel.y))<obj_wheel.halfsize){
        hspd = 0;
        vspd = 0;
        visible = obj_wheel.visible;
    }
    x += hspd;
    y += vspd;
}else{
    image_speed = 0;
    scr_insect_hooking();
}

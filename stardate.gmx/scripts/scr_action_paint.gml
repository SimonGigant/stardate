///scr_action_paint()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,1);

with(obj_wheel_parent){
    current_sprite_action = spr_action_touch;
}

with(obj_jauge_soumission){
    submission -= 2;
}
with(obj_jauge_respect){
    respect += 4;
}
with(obj_jauge_amour){
    love += 1;
}

//effets :

ef_sub = MINUS;
ef_resp = PLUS;
ef_love = PLUS;

///scr_action_cook()

//Ici les effets de cette action sur l'ennemi

scr_arrow_launch(RESPECT,3);

with(obj_wheel_parent){
    scr_changing_direction();
    current_sprite_action = spr_action_cook;
}

with(obj_jauge_soumission){
    submission += 0;
}
with(obj_jauge_respect){
    respect += 3;
}
with(obj_jauge_amour){
    love += 3;
}

//effets :

ef_sub = EQUAL;
ef_resp = PLUS;
ef_love = PLUS;
